db.rooms.insertOne({
	name: "single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
});

db.rooms.insertMany([
	{
	name: "double",
	accommodates: 3,
	price: 2000,
	description: "A room fit for a small",
	rooms_available: 5,
	isAvailable: false
	},
	{
	name: "queen",
	accommodates: 4,
	price: 4000,
	description: "A room with a queen sized bed perfect for a simple getaway",
	rooms_available: 15,
	isAvailable: false	
	}
]);

db.users.updateOne(
	{ name: "queen"},
	{
		$set: {
			name: "queen",
			accommodates: 4,
			price: 4000,
			description: "A room with a queen sized bed perfect for a simple getaway",
			rooms_available: 0,
			isAvailable: false	

		}
	} 
);

db.users.deleteMany({
	rooms_available: 0,
});


db.rooms.find();
